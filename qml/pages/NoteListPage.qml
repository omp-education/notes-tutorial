import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    Column {
        anchors.fill: parent

        PageHeader { title: qsTr("Notes") }
        ListItem {
            property string noteTitle: qsTr("Note 1")
            property string noteDescription: qsTr("Note 1 description. It could be really long and have several lines.")

            // ToDo: show title and description
            // ToDo: open note details when clicked
        }
        // ToDo: create 2 new note items
    }
}
