TARGET = notes-tutorial

CONFIG += sailfishapp

SOURCES += src/notes-tutorial.cpp

DISTFILES += qml/notes-tutorial.qml \
    qml/cover/CoverPage.qml \
    qml/pages/NoteListPage.qml \
    qml/pages/NoteDetailsPage.qml \
    rpm/notes-tutorial.yaml \
    translations/*.ts \
    notes-tutorial.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += sailfishapp_i18n
TRANSLATIONS += translations/notes-tutorial-ru.ts
